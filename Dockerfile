FROM ubuntu:19.04

RUN apt-get update && \
    apt-get install -y nodejs nodejs-doc neovim neovim-runtime python3-neovim npm 
RUN  npm install npm --global && npm install -g ember-cli